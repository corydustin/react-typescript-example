# Example React TypeScript Project

Quick project thrown together to understand React/Redux with TypeScript further.

# Getting Started

To get this project up and running locally please utilizing the following steps: 
1. Install the latest version of Node, please see the following link for how to accomplish this: https://nodejs.org/en/download/
2. Next, within the root of the project directory run `npm install` to install all node packages to your machine. 
3. Finally simply run `npm start` to run the project server locally. The project should open a brower on port 3000. 
4. You're ready to rock and roll!`