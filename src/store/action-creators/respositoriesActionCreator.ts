import axios from 'axios';
import { Dispatch } from 'redux';

import { RepositoriesActionType } from '../action-types';
import { RepositoriesAction } from '../actions'; 

export const searchRespositories = (term: string) => {

  return async (dispatch: Dispatch<RepositoriesAction>) => {

    // Store that we are attempting to search for a repository
    dispatch({
      type: RepositoriesActionType.SEARCH_REPOSITORIES
    });

    try {
      const { data } = await axios.get('https://registry.npmjs.org/-/v1/search', {
        params: {
          text: term
        }
      });

      // Search for names and dispatch results when found of repositories
      const names = data.objects.map((result: any) => {
        return result.package.name
      });

      dispatch({
        type: RepositoriesActionType.SEARCH_REPOSITORIES_SUCCESS,
        payload: names
      })

    } catch(error: any) {
      // There was an error, store error and continue
      dispatch({
        type: RepositoriesActionType.SEARCH_REPOSITORIES_ERROR,
        payload: error.message
      })
    }

  };

}