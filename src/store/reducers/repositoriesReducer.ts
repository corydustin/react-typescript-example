import { RepositoriesActionType } from '../action-types';
import { RepositoriesAction } from '../actions';

interface RepositoriesState {
  loading: Boolean; 
  error: string | null;
  data: string[];
}

// Initial state for reducer
const initialState = {
  loading: false,
  error: null,
  data: []
}

const reducer = (
  state: RepositoriesState = initialState, 
  action: RepositoriesAction
): RepositoriesState => {

  switch(action.type) {
    case RepositoriesActionType.SEARCH_REPOSITORIES:
      return {
        loading: true,
        error: null,
        data: []
      };
    case RepositoriesActionType.SEARCH_REPOSITORIES_SUCCESS:
      return {
        loading: false,
        error: null,
        data: action.payload
      }
    case RepositoriesActionType.SEARCH_REPOSITORIES_ERROR:
      return {
        loading: false,
        error: action.payload,
        data: []
      }
    default: 
      return state;
  }

}

export default reducer