import { RepositoriesActionType } from "../action-types";

interface SearchRepositoriesAction {
  type: RepositoriesActionType.SEARCH_REPOSITORIES;
}

interface SearchRepositoriesSuccessAction {
  type: RepositoriesActionType.SEARCH_REPOSITORIES_SUCCESS;
  payload: string[] 
}

interface SearchRespositoriesErrorAction {
  type: RepositoriesActionType.SEARCH_REPOSITORIES_ERROR;
  payload: string;
}

export type RepositoriesAction = 
SearchRepositoriesAction | 
SearchRepositoriesSuccessAction | 
SearchRespositoriesErrorAction;