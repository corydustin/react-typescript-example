import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';

import { actionCreators } from '../store';

// Hook to use any action we have defined within project without actually calling dispatch
export const useActions = () => {

  const dispatch = useDispatch();

  return bindActionCreators(actionCreators, dispatch);
};